const phpRequestTimerDelay = 1000;

function restrictInputLength(input, maxLength) {
    if (input.value.length > maxLength) {
        input.value = input.value.slice(0, input.value.length - 1);
    }
}

function ajaxConnErrorRedirect() {
    console.error("[AJAX] Connection Error");
    alert("Erreur de connexion. Vous allez être redirigé vers la page d'accueil.");
    window.location.replace("http://localhost/BattleshipOnline/public/index.php");
}