let statusChecker = undefined;
/* JOIN ROOM */

let joinRoomDiv = document.getElementById("div-join-room");
let joinRoomInput = document.getElementById("input-join-key");
let isJoinRoomOpen = true;

function joinRoom() {
    if (statusChecker !== undefined) {
        clearInterval(statusChecker);
        statusChecker = undefined;
    }
    if (isJoinRoomOpen) {
        if (joinRoomInput.value.trim().length !== 0) {
            $.ajax({
                type: "GET",
                url: '../php/joinroom.php',
                data: {
                    roomKey: joinRoomInput.value
                },
                success: function() {
                    console.log("[AJAX] Room joined");
                  },
                error: function() {
                    ajaxConnErrorRedirect();
                }
            }).done(function (data) {
                if (data != "null") {
                    console.log(data);
                    let availableRoom = JSON.parse(data);
                    if (availableRoom.roomKey === joinRoomInput.value) {
                        $('#span-room-status1').html("Room available");
                        $.ajax({
                            type: "POST",
                            url: '../php/createsession.php',
                            data: {
                                roomID: availableRoom.roomID,
                                roomKey: availableRoom.roomKey
                            },
                            success: function() {
                                console.log("[AJAX] Game session created");
                            },
                            error: function() {
                                ajaxConnErrorRedirect();
                            }
                        }).done(function(data) {
                            window.location.pathname = "/BattleshipOnline/public/game.php";
                        });
                    }
                } else {
                    $('#span-room-status').html("Room not available");
                    joinRoomInput.value = "";
                }
                
            })
        }
    }

    createRoomDiv.style.display = "none";
    joinRoomDiv.style.display = "block";
    isJoinRoomOpen = true;
    isCreateRoomOpen = false;
}

/* CREATE ROOM */

let createRoomDiv = document.getElementById("div-create-room");
let isCreateRoomOpen = false;

function createRoom() {
    joinRoomDiv.style.display = "none";
    createRoomDiv.style.display = "block";

    $.ajax({
        type: "GET",
        url: '../php/createroom.php',
        dataType: "html",
        success: function() {
            console.log("[AJAX] Room created");
          },
        error: function() {
            ajaxConnErrorRedirect();
        }
    }).done(function (data) {
        $('#span-room-key').html(data);
        statusChecker = setInterval(checkRoomStatus, 1000);
    });

    isCreateRoomOpen = true;
    isJoinRoomOpen = false;
}

function checkRoomStatus() {
    $.ajax({
        type: "GET",
        url: '../php/checkroomstatus.php',
        dataType: "html",
        success: function() {
            console.log("[AJAX] Checking room status");
          },
        error: function() {
            ajaxConnErrorRedirect();
        }
    }).done(function (data) {
        if (data == "1") {
            $('#span-room-status2').html("Player found");
            window.location.pathname = "/BattleshipOnline/public/game.php";
        }
    });
}