<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="author" content="Marc Meynet">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Battleship Online</title>
    <link rel="stylesheet" href="../public/styles.css">
    <link rel="icon" type="image/png" sizes="48x48" href="../img/battleship-favicon-48px.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../img/battleship-favicon-32px.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../img/battleship-favicon-16px.png">
    <link rel="shortcut icon" href="../img/battleship-favicon.ico">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
</head>
<body>