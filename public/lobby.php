<?php
require_once '../php/userlogin.php';
include_once '../php/includes/head-inc.php';
include_once '../php/includes/header-inc.php';
if(session_id() == '') {
    session_start();
}
$_SESSION['amIPlaying'] = 0;
?>
    <main id="lobby-container" class="container">
        <div id="div-join-room" class="lobby-rooms">
            <label for="input-join-key" class="lobbyDivs-text">Entrer le code de la partie </label>
            <input type="text" id="input-join-key" name="input-join-key" placeholder="1QA82B" onkeyup="restrictInputLength(this, 6)">
            <span id="span-room-status1" class="lobbyDivs-text"></span>
        </div>
        <div id="div-create-room" class="lobby-rooms">
            <p class="lobbyDivs-text">Voici le code de la partie</p>
            <span id="span-room-key" class="lobbyDivs-text"></span>
            <span id="span-room-status2" class="lobbyDivs-text"></span>
        </div>

        <div id="lobby-btn-container">
            <button id="btn-join-room" onclick="joinRoom()">Rejoindre une partie</button>
            <button id="btn-create-room" onclick="createRoom()">Créer une partie</button>
        </div>
    </main>

    <?php
    include_once '../php/includes/footer-inc.php';
    ?>

    <script defer type="application/javascript" src="../js/globals.js"></script>
    <script defer type="application/javascript" src="../js/lobby.js"></script>
</body>
</html>