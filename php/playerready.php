<?php
    ini_set('display_errors', 1);
    require_once 'includes/connect-inc.php';

    $roomID = $_SESSION['roomID'];
    $userID = isset($_GET['userID']) ? $_GET['userID'] : "ERROR";
    $_SESSION['userID'] = $userID;

    $dbh->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    try {
        $sqlSelect = "SELECT * FROM Rooms WHERE roomID = $roomID";
        $sth = $dbh->query($sqlSelect);
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $row) {
            if($row['gameEnded'] == 1) {
                echo 1;
            } else if($row['user2_ID'] != null) {
                if($row['user2_ID'] != $userID) {
                    $_SESSION['player2ID'] = $row['user2_ID'];
                    $sqlInsert = "UPDATE Rooms SET user1_Ready = 1 WHERE roomID = $roomID";
                    $result2 = $dbh->exec($sqlInsert);

                    if (!$result2) {
                        print "Failed to get ready: " . $conn->errorMsg();
                    }
                } else {
                    $_SESSION['player2ID'] = $row['user1_ID'];
                    $sqlInsert = "UPDATE Rooms SET user2_Ready = 1 WHERE roomID = $roomID";
                    $result2 = $dbh->exec($sqlInsert);

                    if (!$result2) {
                        print "Failed to get ready: " . $conn->errorMsg();
                    }
                }
            } else {
                echo 0;
            }
        }
    } catch(PDOException $e) {
        echo $e->getMessage();
    }

    $dbh = null;
?>