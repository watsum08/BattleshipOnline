<?php
include_once '../php/includes/head-inc.php';
include_once '../php/includes/header-inc.php';
if(session_id() == '') {
    session_start();
}
if (isset($_SESSION['amIPlaying'])) {
    if ($_SESSION['amIPlaying'] == 1) {
        header("Location: http://localhost/BattleshipOnline/public/index.php");
        exit();
    } else {
        $_SESSION['amIPlaying'] = 1;
    }
} else {
    header("Location: http://localhost/BattleshipOnline/public/index.php");
    exit();
}
?>
    <main id="game-container">
        <div id="gameInfo">
            <div id="textToSpeech-div">
                <label for="textToSpeech-checkbox">Lecteur vocale: </label>
                <input type="checkbox" id="textToSpeech-input" name="textToSpeech-input">
            </div><br>
            Temps restant: <span id="timeLeft-span">--</span><br><br>
            Navires alliés restants: <span id="playerShipsLeft-span"></span><br>
            Navires ennemis restants: <span id="enemyShipsLeft-span"></span><br><br>
            État de jeu: <br>
            <strong><span id="gameStatus-span"></span></strong><br><br>
            <input type="hidden" id="userID-input" value="<?php print $_SESSION['userID'] ?>">
            <button id="surrender-btn" onclick="gameEnd(0)">Abandonner la partie</button>
        </div>
        <div id="playerZone" class="gameZone"></div>
        <div id="enemyZone" class="gameZone"></div>
    </main>

    <?php
    include_once '../php/includes/footer-inc.php';
    ?>
    
    <script defer type="application/javascript" src="../js/globals.js"></script>
    <script defer type="application/javascript" src="../js/game.js"></script>
</body>
</html>