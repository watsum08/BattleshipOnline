<?php
    ini_set('display_errors', 1);
    require_once 'includes/connect-inc.php';

    $userID = isset($_SESSION['userID']) ? $_SESSION['userID'] : "ERROR";
    $roomID = isset($_SESSION['roomID']) ? $_SESSION['roomID'] : "ERROR";

    $dbh->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    try {
        $sqlSelect2 = "SELECT * FROM Moves WHERE roomID = $roomID ORDER BY moveID DESC LIMIT 1";
        $sth2 = $dbh->query($sqlSelect2);
        $result2 = $sth2->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result2 as $row) {
            if($row['playerID'] != $userID) {
                $enemyShipsLeft = $row['myShipsLeft'];
                echo $enemyShipsLeft;
            } else {
                echo 0;
            }
        }
    } catch(PDOException $e) {
        echo $e->getMessage();
    }

    $dbh = null;
?>