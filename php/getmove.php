<?php
    ini_set('display_errors', 1);
    require_once 'includes/connect-inc.php';

    $userID = isset($_SESSION['userID']) ? $_SESSION['userID'] : "ERROR";
    $roomID = isset($_SESSION['roomID']) ? $_SESSION['roomID'] : "ERROR";

    $dbh->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    try {
        $sqlSelect = "SELECT * FROM Rooms WHERE roomID = $roomID";
        $sth = $dbh->query($sqlSelect);
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $row) {
            if($row['gameEnded'] == 1) {
                $casePosition = "END";
                echo $casePosition;
                $gameEnded = 1;
            } else {
                $gameEnded = 0;
            }
        }
        if ($gameEnded != 1) {
            $sqlSelect2 = "SELECT * FROM Moves WHERE roomID = $roomID ORDER BY moveID DESC LIMIT 1";
            $sth2 = $dbh->query($sqlSelect2);
            $result2 = $sth2->fetchAll(PDO::FETCH_ASSOC);
            foreach ($result2 as $row) {
                if($row['playerID'] != $userID) {
                    $casePosition = $row['casePosition'];
                    echo $casePosition;
                } else {
                    echo 0;
                }
            }
        }
    } catch(PDOException $e) {
        echo $e->getMessage();
    }

    $dbh = null;
?>