<?php
    ini_set('display_errors', 1);
    require_once 'includes/connect-inc.php';

    $roomID = isset($_SESSION['roomID']) ? $_SESSION['roomID'] : "ERROR";

    $dbh->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    try {
        $sqlSelect = "SELECT * FROM Rooms WHERE roomID = $roomID";
        $sth = $dbh->query($sqlSelect);
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $row) {
            if ($row['startingPlayer'] == 0) {
                echo $row['user1_ID'];
            } else {
                echo $row['user2_ID'];
            }
        }
    } catch(PDOException $e) {
        echo $e->getMessage();
    }
?>