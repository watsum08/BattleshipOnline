<?php
    ini_set('display_errors', 1);
    require_once 'includes/connect-inc.php';
    $userID = $_SESSION['userID'];

    function joinRoom($dbh, $userID) {
        $dbh->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        try {
            $roomKey = isset($_GET['roomKey']) ? $_GET['roomKey'] : "ERROR";
            $roomFound = 0;

            $sqlSelect = "SELECT * FROM Rooms;";
            $sth = $dbh->query($sqlSelect);
            $result = $sth->fetchAll(PDO::FETCH_ASSOC);
            foreach ($result as $row) {
                if ($row['roomKey'] == $roomKey && $row['gameEnded'] == 0 && $row['user2_ID'] == null) {
                    $availableRoom = [
                        "roomID" => $row['roomID'],
                        "roomKey" => $row['roomKey']
                    ];
                    $roomID = $row['roomID'];
                    $sql2 = "UPDATE Rooms SET user2_ID = $userID WHERE roomID = $roomID";
                    $result2 = $dbh->exec($sql2);
                    echo json_encode($availableRoom);
                    $roomFound++;
                    break;
                }
            }
            if ($roomFound == 0) {
                echo "null";
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }

        $dbh = null;
    }

    joinRoom($dbh, $userID);
?>