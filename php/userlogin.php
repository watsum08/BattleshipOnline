<?php
ini_set('display_errors', 1);
require_once 'includes/connect-inc.php';

if (isset($_POST['input-username'])) {
    $inputUsername = $_POST['input-username'];
    if (ctype_space($inputUsername)) {
        $inputUsername = "NoName";
    }
    $_SESSION['username'] = $inputUsername;
} else if (isset($_SESSION['username'])) {
    $inputUsername = $_SESSION['username'];
} else {
    header("Location: http://localhost/BattleshipOnline/public/index.php");
    exit();
}

$clientIpAddress = $_SERVER['REMOTE_ADDR'];
if ($clientIpAddress == "::1") { $clientIpAddress = "localhost"; }
$_SESSION['ipAddress'] = $clientIpAddress;

$userID = null;

$dbh->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    try {
        $sqlSelect = "SELECT * FROM Users;";
        $sth = $dbh->query($sqlSelect);
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $row) {
            if($row['username'] == $inputUsername) {
                $sqlSelect = "SELECT userID FROM Users WHERE username = '$inputUsername'";
                $sth2 = $dbh->query($sqlSelect);
                $userID = $sth2->fetchColumn();
                break;
            }
        }
        if ($userID == null) {
            $sqlInsert = "INSERT INTO Users (username, ipAddress) VALUES ('$inputUsername', '$clientIpAddress');";
            $execute = $dbh->exec($sqlInsert);
            $sqlSelect = "SELECT userID FROM Users WHERE username = '$inputUsername'";
            $sth2 = $dbh->query($sqlSelect);
            $userID = $sth2->fetchColumn();
        }
        $_SESSION['userID'] = $userID;
    } catch(PDOException $e) {
        echo $e->getMessage();
    }

$dbh = null;
?>