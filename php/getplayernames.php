<?php
    ini_set('display_errors', 1);
    require_once 'includes/connect-inc.php';

    $roomID = isset($_SESSION['roomID']) ? $_SESSION['roomID'] : "ERROR";
    $userID = isset($_SESSION['userID']) ? $_SESSION['userID'] : "ERROR";
    $myUsername = isset($_SESSION['username']) ? $_SESSION['username'] : "ERROR";
    $player2ID = isset($_SESSION['player2ID']) ? $_SESSION['player2ID'] : "ERROR";

    $dbh->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    try {
        $sqlSelect = "SELECT * FROM Users";
        $sth = $dbh->query($sqlSelect);
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $row) {
            if ($player2ID == $row['userID']) {
                $enemyUsername = $row['username'];
            }
        }
        $usernames = array($myUsername, $enemyUsername);
        echo json_encode($usernames);
    } catch(PDOException $e) {
        echo $e->getMessage();
    }

    $dbh = null;
?>