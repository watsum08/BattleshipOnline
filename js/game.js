/* PAGE STRUCTURE/CREATION */
let playerZone = document.getElementById('playerZone');
let enemyZone = document.getElementById('enemyZone');

for (let i = 0; i < 100; i++) {
    let playerDivBlock = document.createElement('button');
    playerDivBlock.classList.add('player-block');
    playerDivBlock.classList.add('game-block');
    playerZone.appendChild(playerDivBlock);

    let enemyDivBlock = document.createElement('button');
    enemyDivBlock.classList.add('enemy-block');
    enemyDivBlock.classList.add('game-block');
    enemyDivBlock.classList.add('block-off');
    enemyZone.appendChild(enemyDivBlock);
}

let playerBlocks = document.getElementsByClassName('player-block');
let enemyBlocks = document.getElementsByClassName('enemy-block');

$(document).ready(function() {
    for (let a = 0; a < 10; a++) {
        for (let b = 0; b < 10; b++) {
            playerBlocks[a * 10 + b].id = (a * 10 + b);
            playerBlocks[a * 10 + b].position = String.fromCharCode(65 + a) + (b + 1).toString();
            enemyBlocks[a * 10 + b].id = (a * 10 + b);
            enemyBlocks[a * 10 + b].position = String.fromCharCode(65 + a) + (b + 1).toString();
        }
    }
});

let textToSpeechCheckbox = document.getElementById('textToSpeech-input');
textToSpeechCheckbox.addEventListener('click', activateTextToSpeech, false);

let speech = new SpeechSynthesisUtterance();
speech.lang = "fr-FR";
let isTextToSpeechOn = false;

function activateTextToSpeech() {
    if (textToSpeechCheckbox.checked)
    {
        let text = "Bientôt disponible";
        speech.text = text;
        window.speechSynthesis.cancel();
        window.speechSynthesis.speak(speech);
        alert(text);
        isTextToSpeechOn = true;
    } else {
        isTextToSpeechOn = false;
    }
}

function textToSpeech(event) {
    if (isTextToSpeechOn) {
        let playerBlock = event.currentTarget;

        console.log("Position alliée " + playerBlock.position);
        speech.text = "Position alliée " + playerBlock.position;
        window.speechSynthesis.cancel();
        window.speechSynthesis.speak(speech);
    }
}

/* GAME LOGIC*/
/* Variables */
let userIDinput = document.getElementById('userID-input');
let userID = userIDinput.value;
let timeLeftSpan = document.getElementById('timeLeft-span');
let playerShipsLeftSpan = document.getElementById('playerShipsLeft-span');
let enemyShipsLeftSpan = document.getElementById('enemyShipsLeft-span');
let gameStatusSpan = document.getElementById('gameStatus-span');

let imFirstPlayer = undefined;
let myUsername = undefined;
let enemyUsername = undefined;
let checkEnemyStatusTimer = undefined;
let playerDestroyedCase = undefined;
let sendNoMoveTimer = undefined;
let getMoveTimer = undefined;
let timeLeftSeconds = undefined;
let timeLeftTimer = undefined;
let player2ID = undefined;

let playerShips = [];
let playerShipsLeft = undefined;
let enemyShips = [];
let enemyShipsLeft = undefined;

function checkDestroyedCases() {
    for (let i = 0; i < 100; i++) {
        if (playerBlocks[i].destroyed === true) {
            playerBlocks[i].classList.add("block-destroyed");
            playerBlocks[i].destroyed = true;
            if (playerShips.includes(playerBlocks[i].position) && playerBlocks[i].killed != true) {
                playerBlocks[i].killed = true;
                playerBlocks[i].classList.add("player-block-killed");
                --playerShipsLeft;
            }
        }
        if (enemyBlocks[i].destroyed === true) {
            enemyBlocks[i].classList.add("block-destroyed");
            enemyBlocks[i].destroyed = true;
        }
    }
    playerShipsLeftSpan.innerHTML = playerShipsLeft.toString();
    if (playerShipsLeft <= 0) {
        gameEnd(0);
    }
}

function gameEnd(imTheWinner) {
    $.ajax({
        type: "POST",
        url: '../php/sendresults.php',
        data: {
            userID: userID,
            imTheWinner: imTheWinner
        },
        success: function() {
            if (imTheWinner == 0) {
                alert("Vous avez perdu la bataille. Noob.");
            } else if (imTheWinner == 1) {
                alert("Vous avez gagné la bataille. Bravo.");
            }
            window.location.replace("http://localhost/BattleshipOnline/public/lobby.php");
          },
        error: function() {
            ajaxConnErrorRedirect();
        }
    });
}

function enemyClick(event) {
    let enemyBlock = event.currentTarget;
    $.ajax({
        type: "POST",
        url: '../php/sendmove.php',
        data: {
            userID: userID,
            casePosition: enemyBlock.position,
            playerShipsLeft: playerShipsLeft
        },
        success: function() {
            for (let i = 0; i < 100; i++) {
                enemyBlocks[i].removeEventListener('click', enemyClick, false);
                enemyBlocks[i].classList.add("block-off");
            }
            enemyBlocks[enemyBlock.id].destroyed = true;
            getMoveTimer = setInterval(getMove, phpRequestTimerDelay);
            checkDestroyedCases();
            console.log("[AJAX] Send move");
            console.log("Enemy position " + enemyBlock.position + " destroyed");

            clearTimeout(sendNoMoveTimer);
            sendNoMoveTimer = undefined;
          },
        error: function() {
            ajaxConnErrorRedirect();
        }
    });
}

function sendNoMove() {
    $.ajax({
        type: "POST",
        url: '../php/sendmove.php',
        data: {
            userID: userID,
            casePosition: "NAN",
            playerShipsLeft: playerShipsLeft
        },
        success: function() {
            for (let i = 0; i < 100; i++) {
                enemyBlocks[i].removeEventListener('click', enemyClick, false);
                enemyBlocks[i].classList.add("block-off");
            }
            getMoveTimer = setInterval(getMove, phpRequestTimerDelay);
            checkDestroyedCases();
            console.log("[AJAX] Send move");
            console.log("No enemy block destroyed");

            clearTimeout(sendNoMoveTimer);
            sendNoMoveTimer = undefined;
          },
        error: function() {
            ajaxConnErrorRedirect();
        }
    });
}

function timeLeftDecrement() {
    if (sendNoMoveTimer != undefined) {
        --timeLeftSeconds;
        timeLeftSpan.innerHTML = timeLeftSeconds.toString();
    } else {
        timeLeftSpan.innerHTML = "--";
        clearInterval(timeLeftTimer);
    }
}

function playerDestroy(casePosition) {
    for (let i = 0; i < 100; i++) {
        if (playerBlocks[i].position === casePosition) {
            playerBlocks[i].destroyed = true;
        }
    }
    checkDestroyedCases();
    prepareMove();
}

function prepareMove() {
    for (let i = 0; i < 100; i++) {
        enemyBlocks[i].classList.remove("block-off");
        if (enemyBlocks[i].destroyed !== true) {
            enemyBlocks[i].addEventListener('click', enemyClick, false);
        }
    }
    timeLeftSeconds = 10;
    timeLeftTimer = setInterval(timeLeftDecrement, 1000);
    sendNoMoveTimer = setTimeout(sendNoMove, 10000);
    $('#activePlayer-username-span').text(myUsername)
    gameStatusSpan.innerHTML = "Votre tour de jouer";
}

function getMove() {
    $.ajax({
        type: "GET",
        url: '../php/getmove.php',
        dataType: "html",
        success: function() {
            console.log("[AJAX] Get move");
          },
        error: function(error) {
            console.error(JSON.stringify(error, null, 2));
            ajaxConnErrorRedirect();
        }
    }).done(function (data) {
        if (data != 0) {
            clearInterval(getMoveTimer);
            if (data == "END") {
                gameEnd(1);
                return;
            } else if (data == "NAN") {
                playerDestroy(data);
                console.log("Enemy didn't destroy any case");
            } else {
                playerDestroy(data);
                console.log("Player position " + data + " destroyed");
            }
            $.ajax({
                type: "GET",
                url: '../php/getenemyshipsleft.php',
                dataType: "html",
                success: function() {
                    console.log("[AJAX] Get enemy ships left");
                  },
                error: function(error) {
                    console.error(JSON.stringify(error, null, 2));
                    ajaxConnErrorRedirect();
                }
            }).done(function (data) {
                if (data != 0) {
                    enemyShipsLeft = data;
                    enemyShipsLeftSpan.innerHTML = enemyShipsLeft.toString();
                } else {
                    console.error("getenemyshipsleft error");
                }
            });
        }
    });

    if (gameStatusSpan.innerHTML != "L'ennemi prépare son attaque") { gameStatusSpan.innerHTML = "L'ennemi prépare son attaque"; };
    $('#activePlayer-username-span').text(enemyUsername);
}

function checkEnemyStatus() {
    $.ajax({
        type: "GET",
        url: '../php/checkenemystatus.php',
        dataType: "html",
        success: function() {
            console.log("[AJAX] Check enemy status");
          },
        error: function() {
            ajaxConnErrorRedirect();
        }
    }).done(function (data) {
        if (data !== "0") {
            player2ID = data;
            enemyShipsLeft = 10;
            enemyShipsLeftSpan.innerHTML = enemyShipsLeft.toString();
            amIFirstPlayer();
            clearInterval(checkEnemyStatusTimer);
        }
    });
}

function amIFirstPlayer() {
    $.ajax({
        type: "GET",
        url: '../php/getfirstplayer.php',
        dataType: "html",
        success: function() {
            console.log("[AJAX] Room created");
          },
        error: function() {
            ajaxConnErrorRedirect();
        }
    }).done(function (data) {
        if (userID == data) {
            imFirstPlayer = true;
        } else {
            imFirstPlayer = false;
        }
        getPlayerNames();
    });
}

function getPlayerNames() {
    $.ajax({
        type: "GET",
        url: '../php/getplayernames.php',
        dataType: "json",
        success: function() {
            console.log("[AJAX] Get player names");
          },
        error: function() {
            ajaxConnErrorRedirect();
        }
    }).done(function (data) {
        myUsername = data[0];
        enemyUsername = data[1];
        console.log("myUsername: " + myUsername);
        console.log("enemyUsername: " + enemyUsername);
        main();
    });
}

function placeAllyShip(event) {
    let playerBlock = event.currentTarget;
    playerShips.push(playerBlock.position);

    playerBlocks[playerBlock.id].classList.add('block-placed');
    playerBlocks[playerBlock.id].removeEventListener('click', placeAllyShip, false);
    console.log(playerShips);
    if (playerShips.length < 10) {
        playerShipsLeftSpan.innerHTML = (parseInt(playerShipsLeftSpan.innerHTML) + 1).toString();
    } else {
        for (let i = 0; i < 100; i++) {
            if (!playerBlocks[i].classList.contains('block-placed')) {
                playerBlocks[i].classList.add('player-block-ready');
            }
            playerBlocks[i].removeEventListener('click', placeAllyShip, false);
        }
        playerShipsLeftSpan.innerHTML = (parseInt(playerShipsLeftSpan.innerHTML) + 1).toString();
        playerReady();
    }

    
}

function placeShips() {
    for (let i = 0; i < 100; i++) {
        playerBlocks[i].addEventListener('click', placeAllyShip, false);
        playerBlocks[i].addEventListener('mouseover', textToSpeech, false);
    }
    playerShipsLeftSpan.innerHTML = 0;
    gameStatusSpan.innerHTML = "Placez vos navires";
}

function playerReady() {
    $.ajax({
        type: "GET",
        url: '../php/playerready.php',
        data: {
            userID: userID
        },
        dataType: "html",
        success: function() {
            
          },
        error: function() {
            ajaxConnErrorRedirect();
        }
    }).done(function (data) {
        if (data == 1) {
            gameEnd(1);
        } else {
            gameStatusSpan.innerHTML = "En attente du placement de navires ennemis";
            checkEnemyStatusTimer = setInterval(checkEnemyStatus, phpRequestTimerDelay);
        }
    });
}

function main() {
    console.log("am i first player ? " + imFirstPlayer);

    if (imFirstPlayer) {
        prepareMove();
    } else {
        getMoveTimer = setInterval(getMove, phpRequestTimerDelay);
    }
}

placeShips();