USE BattleshipGame;

SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS Users;
DROP TABLE IF EXISTS Rooms;
DROP TABLE IF EXISTS Moves;
SET FOREIGN_KEY_CHECKS = 1;

CREATE TABLE Users (
    userID int UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
    username varchar(8) NOT NULL,
    ipAddress varchar(15) NOT NULL,
    nbWin int UNSIGNED DEFAULT 0,
    nbLose int UNSIGNED DEFAULT 0,
    gameTime time DEFAULT '00:00:00',
    created_at timestamp DEFAULT CURRENT_TIMESTAMP,
    updated_at datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=INNODB;

CREATE TABLE Rooms (
    roomID int UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
    roomKey varchar(6) NOT NULL,
    user1_ID int UNSIGNED NOT NULL,
    user2_ID int UNSIGNED,
    startingPlayer int UNSIGNED,
    user1_Ready boolean DEFAULT 0,
    user2_Ready boolean DEFAULT 0,
    gameEnded boolean DEFAULT 0,
    CONSTRAINT FK_user1_ID FOREIGN KEY (user1_ID) REFERENCES Users(userID),
    CONSTRAINT FK_user2_ID FOREIGN KEY (user2_ID) REFERENCES Users(userID)
) ENGINE=INNODB;

CREATE TABLE Moves (
    moveID int UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
    roomID int UNSIGNED NOT NULL,
    playerID int UNSIGNED NOT NULL,
    casePosition varchar(3) NOT NULL,
    myShipsLeft int UNSIGNED NOT NULL DEFAULT 10,
    CONSTRAINT FK_roomID FOREIGN KEY (roomID) REFERENCES Rooms(roomID),
    CONSTRAINT FK_playerID FOREIGN KEY (playerID) REFERENCES Users(userID)
) ENGINE=INNODB;