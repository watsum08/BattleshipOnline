# Battleship Online

Auteur : [Marc Meynet](https://gitlab.com/watsum08/BattleshipOnline "@watsum08")

Description : Projet d'école CFC-24 avec le professeur [Jeremy Gobet](https://gitlab.com/PimsJay01 "@PimsJay01")<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Module 151 - Intégrer bases de données dans des applications Web

Début du projet : 2 février 2022<br>
Fin du projet : pas terminé

![Battleship Online Logo](/img/battleship-online-logo-wBoat-892px.png)

## Sommaire
1. [Vision](#vision)
2. [Prototype](#prototype)
    - [Résumé](#resume)
    - [Outils](#outils)
    - [Fonctionnalités](#fonctionnalites)
3. [Liste des communications](#communications)
4. [Base de données](#bd)
5. [Crédits](#credits)

---

## Vision <a name="vision"></a>

Le but de ce projet est de créer un jeu accessible possédant la même difficulté pour les personnes voyants et malvoyants.

Je vise à utiliser un logiciel de synthèse vocale pour décrire l'écran, le jeu, de façon auditive. J'aimerais aussi que ce jeu soit en ligne, que deux personnes puissent joueur l'un contre l'autre, et qu'il aura un système de highscores.

---

## Prototype <a name="prototype"></a>

### Résumé <a name="resume"></a>
                                                                               
J'ai commencé par une version, nommée « The Ghost Game », qui avait pour but de créer un jeu de tir avec une détection d'ennemis sonores (beep lorsqu'on approche la souris de l'ennemi). Mais le jeu ne me plaisait pas, le beep était insupportable et ne me donnait aucune envie de jouer. J'ai donc décidé de recommencer mon projet et partir sur une nouvelle version, celle-ci.
>Pour ceux qui s'intéressent au projet précédent, voici son dépôt GitLab : https://gitlab.com/watsum08/the-ghost-game/
                 
Ce projet nommé **Battleship Online** *(Bataille navale en ligne)* reprend le jeu de société, aussi connu sous le nom de « touché-coulé ». Un jeu auquel deux joueurs placent des navires sur une grille de 100 cases (10x10) et chacun doit faire couler les navires de l'autre. La grille adverse est tenue secrète afin de ne pas connaître où l'adversaire place ses navires.

Chaque tour de jeu dure au maximum 10 secondes pour éviter que les parties durent trop longtemps. Lorsqu'un joueur a détruit tous les navires de son adversaire, la partie se termine et celui-ci aura donc gagné la partie. Le nombre de parties gagnées et perdues *(Win/Lose)* est enregistré dans la base de données et permettra de sauvegarder et afficher le « Win/Lose ratio » des joueurs.                                                     

Le jeu sera disponible sur navigateur Web et sera aussi adapté pour les navigateurs mobiles lorsque la version web sera terminé.


### Outils <a name="outils"></a>

**Logiciels**

- Visual Studio Code
- GIMP
- Apache Web Server
- Git/Gitlab
- Chromium/Firefox

**Langages**

- HTML/CSS
- JavaScript
- PHP/MySQL
- Ajax/jQuery


### Fonctionnalités <a name="fonctionnalites"></a>

**La première page** qui est la page d’accueil permet d’entrer le nom du joueur et lorsqu’on appuie sur le bouton, la page enregistre le nom du joueur dans la base de données et passe à la page suivante. Si la personne a déjà joué, entré son nom précédemment, la page passera son nom ainsi que ses données enregistrées directement à la deuxième page grâce à des sessions/cookies.

**La deuxième page** permet soit de rejoindre une partie avec un code d’accès, soit de créer une partie et ensuite obtenir un code d’accès.

**La troisième page** permet de jouer à la partie créée. Elle affichera les deux grilles, celle du joueur et de son adversaire. Le jeu démarre lorsque les deux joueurs sont connectés à la partie. Chaque tour dure 10 secondes et pendant ces 10 secondes le joueur clique sur une case adverse afin de détruire ses navires. Lorsqu’il aura cliqué sur une case, ou que le temps de tour s’est écoulé, le joueur adverse aura le droit à son tour. Le jeu se termine lorsqu’un joueur ne lui reste plus aucun navire. Le gagnant et perdant sont enregistrés dans la base de données ainsi que le temps de partie. Quand le jeu est terminé, on passe automatiquement à la quatrième page.

**La quatrième page** permet d’afficher les highscores des 10 meilleurs joueurs. Les highscores se basent sur le nombre de partie gagnées et perdues d’un joueur. On peut aussi voir le temps de partie des 10 meilleurs joueurs. Grâce à un bouton, on peut revenir sur la deuxième page et recommencer une nouvelle partie. 

---

## Liste des communications <a name="communications"></a>

| Verbe HTTP | Endpoint  | Données                             | Description                                                                |
|------------|-----------|-------------------------------------|----------------------------------------------------------------------------|
| GET        | Home/     | index.php                           | Récupère la page d'accueil                                                 |
| GET        | Users/    | userID, username, wrRatio, gameTime | Récupère les données d'utilisateur                                         |
| POST       | Users/    | username, ipAddress                 | Crée un nouvel utilisateur + son IP                                        |
| GET        | Lobby/    | lobby.php                           | Récupère la page pour joindre ou créer une partie *(room)*                 |
| GET        | Rooms/    | nbSameRoomKey                       | Vérifie s'il existe un "room" avec le même code d'accès                    |
| POST       | Rooms/    | userID                              | Crée un "room" avec un code d'accès à 6 caractères généré automatiquement  |
| POST       | Rooms/    | roomKey                             | Confirme la présence du 2ème joueur                                        |
| GET        | Rooms/    | player2                             | Vérifie la présence du 2ème joueur                                         |
| GET        | Game/     | game.php                            | Récupère la page de jeu                                                    |
| POST       | Moves/    | caseEnnemi                          | Envoie le choix de case ennemi (position de navire, ex: "A8") à detruire   |
| GET        | Moves/    | caseJoueur                          | Récupère le choix de case allié à détruire                                 |
| POST       | Users/    | userID, nbWin, nbLose, gameTime     | Met à jour le score et temps de partie de l'utilisateur                    |
| GET        | Users/    | username, wrRatio, gameTime         | Récupère les 10 meilleurs highscores pour les afficher                     |

---

## Base de données <a name="bd"></a>

<table>
<tr><td>

| Users |
|------|
| userID int *PK* |
| username varchar(8) |
| ipAddress varchar(15) |
| nbWin int |
| nbLose int |
| gameTime time |
| created_at timestamp |
| updated_at datetime |

</td><td>

| Rooms |
|-------|
| roomID int *PK* | 
| roomKey varchar(6) |
| user1_ID int *FK* |
| user2_ID int *FK* |
| startingPlayer int |
| user1_Ready boolean |
| user2_Ready boolean |
| gameEnded boolean |

</td><td>

| Moves |
|-------|
| moveID int *PK* |
| roomID int *FK* |
| playerID int *FK* |
| casePosition varchar(3) |

</td></tr> 
</table>

## Crédits <a name="credits"></a>

Merci à tous les contributeurs qui m'ont permis de créer ce projet.

### Image du navire de logo
Auteur : Jade Pennig<br>
Lien : https://dribbble.com/shots/3364653-Battleship-Logo-SVG-Concept-Art

### Images des navires de jeu

Auteur : Lowder2<br>
Lien : https://opengameart.org/content/sea-warfare-set-ships-and-more