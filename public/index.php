<?php
include_once '../php/includes/head-inc.php';
?>
    <main id="index-container" class="container">
        <img src="../img/battleship-online-logo-wBoat.png" alt="Battleship Online Logo">
        <form id="div-join-room" class="lobby-rooms" action="lobby.php" method="post">
            <label for="input-join-key" class="lobbyDivs-text">Entrer votre nom: </label>
            <input type="text" id="input-username" name="input-username" onsubmit="checkEmptyString()" onkeyup="restrictInputLength(this, 8)">
            <input type="submit" value="GO">
        </form>
    </main>

    <?php
    include_once '../php/includes/footer-inc.php';
    ?>

    <script defer type="application/javascript" src="../js/globals.js"></script>
    <script defer type="application/javascript" src="../js/index.js"></script>
</body>
</html>