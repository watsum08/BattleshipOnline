<?php
if(session_id() == '') {
    session_start();
}

if (!$userID) {
    $userID = isset($_SESSION['userID']) ? $_SESSION['userID'] : "ERROR";
}
$ipaddress = isset($_SESSION['ipAddress']) ? $_SESSION['ipAddress'] : "ERROR";

require 'connect-inc.php';

$dbh->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    try {
        $sqlSelect = "SELECT * FROM Users WHERE userID = $userID";
        $sth = $dbh->query($sqlSelect);
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $row) {
            $username = $row['username'];
            $nbWin = $row['nbWin'];
            $nbLose = $row['nbLose'];
        }
    } catch(PDOException $e) {
        echo $e->getMessage();
    }
if ($nbLose > 0 && $nbWin > 0) {
    $wrRatio = sprintf('%.2f', $nbWin / $nbLose);
} else if ($nbLose > 0) {
    $wrRatio = sprintf('%.2f', -$nbLose);
} else {
    $wrRatio = sprintf('%.2f', $nbWin);
}
?>
<header>
    <div>Nom d'utilisateur: <?php print $username; ?> &nbsp;&nbsp;&nbsp;
    Adresse IP: <?php print $ipaddress; ?> &nbsp;&nbsp;&nbsp;
    Victoires: <?php print $nbWin; ?> &nbsp;&nbsp;&nbsp;
    Défaites: <?php print $nbLose; ?> &nbsp;&nbsp;&nbsp;
    Ratio: <?php print $wrRatio ?> </div><br>
    <strong>Ne pas rafraîchir la page</strong>
</header>