<?php
    ini_set('display_errors', 1);
    require_once 'includes/connect-inc.php';
    $userID = $_SESSION['userID'];

    function createRandomKey() {
        $letters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $keyLength = 6;
        $randomKey = '';

        for ($i = 0; $i < $keyLength; $i++) {
            $randomKey .= $letters[rand(0, strlen($letters) - 1)];
        }

        return $randomKey;
    }

    function createRoom($dbh, $userID) {
        $roomKey = null;
        $existingKey = null;

        $dbh->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        try {
            $roomKey = createRandomKey();
            $sqlSelect = "SELECT * FROM Rooms;";
            $sth = $dbh->query($sqlSelect);
            $result = $sth->fetchAll(PDO::FETCH_ASSOC);
            foreach ($result as $row) {
                if($row['roomKey'] == $roomKey) {
                    $roomKey = createRandomKey();
                }
            }
            echo $roomKey;
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
        $sqlInsert = "INSERT INTO Rooms (roomKey, user1_ID) VALUES ('$roomKey', $userID);";
        $execute = $dbh->exec($sqlInsert);
        try {
            $sqlSelect = "SELECT * FROM Rooms";
            $sth = $dbh->query($sqlSelect);
            $result = $sth->fetchAll(PDO::FETCH_ASSOC);
            foreach ($result as $row) {
                if($row['roomKey'] == $roomKey && $row['gameEnded'] == 0) {
                    $_SESSION['roomID'] = $row['roomID'];
                }
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }

        $dbh = null;
    }

    createRoom($dbh, $userID);
?>