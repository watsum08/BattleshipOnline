<?php
    ini_set('display_errors', 1);
    require_once 'includes/connect-inc.php';

    $userID = $_SESSION['userID'];
    $roomID = isset($_SESSION['roomID']) ? $_SESSION['roomID'] : "ERROR";

    $dbh->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    try {
        $sqlSelect = "SELECT * FROM Rooms WHERE roomID = $roomID";
        $sth = $dbh->query($sqlSelect);
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $row) {
            if($row['user2_ID'] != null) {
                if($row['user2_ID'] != $userID && $row['user2_Ready'] == 1) {
                    $_SESSION['player2ID'] = $row['user2_ID'];
                    echo $row['user2_ID'];
                } else if ($row['user2_ID'] == $userID && $row['user1_Ready'] == 1) {
                    $_SESSION['player2ID'] = $row['user1_ID'];
                    echo $row['user1_ID'];
                } else {
                    echo 0;
                }
            } else {
                echo 0;
            }
        }
    } catch(PDOException $e) {
        echo $e->getMessage();
    }
?>